# Build Script

# rhel openjdk install tools
# microdnf install findutils git

export ANDROID_HOME="/opt/android/sdk"
export PATH=$PATH:/opt/android/sdk/cmdline-tools/tools/bin
if [ ! -d "$ANDROID_HOME/cmdline-tools" ]; then
 curl -o sdk.zip https://dl.google.com/android/repository/commandlinetools-linux-7583922_latest.zip
 unzip sdk.zip
 rm sdk.zip
 mkdir "$ANDROID_HOME/cmdline-tools"
 mv cmdline-tools "$ANDROID_HOME/cmdline-tools/tools"
 yes | "$ANDROID_HOME/cmdline-tools/tools/bin/sdkmanager" --licenses
fi

# Clone NewPipeExtractor
# rm -fr NewPipeExtractor
# chmod 755 ./clone.sh && ./clone.sh

# Change dir
cd "NewPipe"

chmod +x gradlew
./gradlew assembleDebug
#./gradlew assembleRelease