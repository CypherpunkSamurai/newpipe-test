# Get release 0.25.0
TAG_1="0.25.0"
wget "https://github.com/TeamNewPipe/NewPipe/archive/refs/tags/v$TAG_1.zip" -O "np.zip"
# Get Commit "7e793c1"
TAG_2="7e793c11aec46358ccbfd8bcfcf521105f4f093a"
wget "https://github.com/TeamNewPipe/NewPipeExtractor/archive/$TAG_2.zip" -O "npe.zip"

# unzip
unzip "np.zip"
unzip "npe.zip"

# remove zip
rm "np.zip" "npe.zip"

# move
mv "NewPipe-$TAG_1" "NewPipe"
mv "NewPipeExtractor-$TAG_2" "NewPipeExtractor"